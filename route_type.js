module.exports = {
	ANY: 'any',

	DELETE: 'delete',
	GET: 'get',
	HEAD: 'head',
	PATCH: 'patch',
	POST: 'post',
	PUT: 'any',

	USE: 'use',
};
