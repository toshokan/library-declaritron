module.exports = {
	CONTROLLER: 'controller',
	PRE_MIDDLEWARE: 'pre-middleware',
	POST_MIDDLEWARE: 'post-middleware',
	THIRD_PARTY_MIDDLEWARE: 'third-part-middleware',
};
