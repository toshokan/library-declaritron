import bodyParser from 'body-parser';
import boolParser from 'express-query-boolean';
import compression from 'compression';
import {Error404} from 'library-common/error';
import express from 'express';
import log from 'library-common/log';
import noop from 'library-common/noop';

import {USE} from '../route_type';

// -----------------------------------------------------------------------------

export default function createServer(routes = [], configureApp = noop) {
	const app = express();
	app.disable('x-powered-by');
	app.disable('etag');
	app.use(compression());
	app.use(bodyParser.json({limit: '6mb'}));
	app.use(bodyParser.urlencoded({extended: false}));
	app.use(boolParser());

	configureApp(app);

	parseRoutes(app, routes);

	app.use(handleThirdPartyOrUnhandledRouteErrors);

	return app;
}

// -----------------------------------------------------------------------------

function parseRoutes(app, definedRoutes = []) {
	definedRoutes.forEach(parseRoute(app));
}

function parseRoute(app) {
	return (route) => {
		const {type} = route;

		if (!app[type]) {
			throw new Error(`Unknown route type: ${type}`);
		}

		return buildRoute(app, route);
	};
}

function buildRoute(
	app,
	{
		type,
		path,
		thirdParty = [],
		pre = [],
		controller: {handler: controller} = {},
		post = [],
	}
) {
	app[type](
		...path ? [path] : [],
		...thirdParty.map((config) => config.handler),
		async (req, res, next = noop) => {
			try {
				await processMiddleware(req, res, pre);

				if (type === USE) {
					await processMiddleware(req, res, post);
					next();
					return;
				}

				const payload = await processController(req, res, controller);
				await processMiddleware(req, res, post);

				processResponse(req, res, get20XCode(payload), payload);
			} catch (error) {
				processError(error, req, res);
			}
		}
	);
}

// -----------------------------------------------------------------------------

function handleThirdPartyOrUnhandledRouteErrors(error, req, res) {
	// unhandled error or unhandled route
	return error instanceof Error
		? processError(error, req, res)
		: processError(new Error404(), error, req);
}

async function processMiddleware(req, res, middlewares = []) {
	for (let index = 0; index < middlewares.length; index += 1) {
		// eslint-disable-next-line  no-await-in-loop
		await middlewares[index].handler(req, res);
	}
}

function processController(req, res, controller) {
	return controller ? controller(req, res) : undefined;
}

function processError(error = {}, req, res) {
	log('#error', error);

	const {
		statusCode = 500,
		message: rawMessage = 'An unknown error occurred.',
		userSafeMessage,
	} = error;
	const message = userSafeMessage || rawMessage;

	return processResponse(
		req,
		res,
		statusCode,
		message ? {message} : undefined
	);
}

function processResponse(req, res, statusCode, payload) {
	res.status(statusCode);

	if (req.newToken) {
		res.set('New-Token', req.newToken);
	}

	return payload ? res.json(payload) : res.end();
}

function get20XCode(payload) {
	return payload === undefined ? 204 : 200;
}
