const {
	CONTROLLER,
	PRE_MIDDLEWARE,
	POST_MIDDLEWARE,
	THIRD_PARTY_MIDDLEWARE,
} = require('./handler_type');

// -----------------------------------------------------------------------------

export function controller(handler) {
	return {
		type: CONTROLLER,
		handler,
	};
}

export function pre(handler) {
	return {
		type: PRE_MIDDLEWARE,
		handler,
	};
}

export function post(handler) {
	return {
		type: POST_MIDDLEWARE,
		handler,
	};
}

export function thirdParty(handler) {
	return {
		type: THIRD_PARTY_MIDDLEWARE,
		handler,
	};
}
