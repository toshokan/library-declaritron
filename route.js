const {
	ANY,
	DELETE,
	GET,
	HEAD,
	PATCH,
	POST,
	PUT,
	USE,
} = require('./route_type');
const {
	CONTROLLER,
	PRE_MIDDLEWARE,
	POST_MIDDLEWARE,
	THIRD_PARTY_MIDDLEWARE,
} = require('./handler_type');
const {throwIf} = require('library-common/throw');

// -----------------------------------------------------------------------------

module.exports = {
	any,
	del,
	get,
	head,
	patch,
	post,
	put,
	use,
};

// -----------------------------------------------------------------------------

function any(path, ...handlers) {
	return route(ANY, path, handlers);
}

function del(path, ...handlers) {
	return route(DELETE, path, handlers);
}

function get(path, ...handlers) {
	return route(GET, path, handlers);
}

function head(path, ...handlers) {
	return route(HEAD, path, handlers);
}

function patch(path, ...handlers) {
	return route(PATCH, path, handlers);
}

function post(path, ...handlers) {
	return route(POST, path, handlers);
}

function put(path, ...handlers) {
	return route(PUT, path, handlers);
}

function use(path, ...handlers) {
	if (typeof path !== 'string') {
		handlers.unshift(path);

		// eslint-disable-next-line no-param-reassign
		path = undefined;
	}

	return route(USE, path, handlers);
}

// -----------------------------------------------------------------------------

// eslint-disable-next-line codemetrics/complexity
function route(type, path, handlers) {
	const config = {
		type,
		path,
		thirdParty: [],
		pre: [],
		controller: undefined,
		post: [],
	};

	return type === USE
		? parseUseHandlers(config, handlers)
		: parseNormalHandlers(config, handlers);
}

// -----------------------------------------------------------------------------

function parseNormalHandlers(config, handlers) {
	handlers.forEach(parseNormalHandler(config));

	throwIf(
		config.controller === undefined,
		`'${config.path}' has no controller`
	);

	return config;
}

function parseNormalHandler(config) {
	const parsers = {
		[CONTROLLER]: parseControllerHandler(config),
		[PRE_MIDDLEWARE]: parsePreMiddlewareHandler(config),
		[POST_MIDDLEWARE]: parsePostMiddlewareHandler(config),
		[THIRD_PARTY_MIDDLEWARE]: parseThirdPartyMiddlewareHandler(config),
	};

	const defaultParser = (handler) => {
		// eslint-disable-next-line no-console
		console.log('Unknown handler type:', handler);

		throw new Error(`'${config.path}' has an unknown handler`);
	};

	return (handler) => {
		const {[handler.type]: parser = defaultParser} = parsers;
		parser(handler);
	};
}

function parseControllerHandler(config) {
	return (handler) => {
		throwIf(
			config.controller !== undefined,
			`'${config.path}' has two many controllers`
		);

		config.controller = handler;
	};
}

function parsePreMiddlewareHandler(config) {
	return (handler) => {
		throwIf(
			config.controller !== undefined,
			`'${
				config.path
			}' has a pre middleware declared after the controller`
		);

		config.pre.push(handler);
	};
}

function parsePostMiddlewareHandler(config) {
	return (handler) => {
		throwIf(
			config.controller === undefined,
			`'${
				config.path
			}' has a post middleware declared before the controller`
		);

		config.post.push(handler);
	};
}

function parseThirdPartyMiddlewareHandler(config) {
	return (handler) => {
		throwIf(
			config.controller !== undefined,
			`'${
				config.path
			}' has a third-party middleware declared after the controller`
		);

		config.thirdParty.push(handler);
	};
}

// -----------------------------------------------------------------------------

function parseUseHandlers(config, handlers) {
	handlers.forEach(parseUseHandler(config));

	return config;
}

function parseUseHandler(config) {
	return (handler) => {
		throwIf(
			handler.type !== PRE_MIDDLEWARE,
			`'${
				config.path
			}' use configuration handler is of type other than PRE_MIDDLEWARE`
		);

		config.pre.push(handler);
	};
}
